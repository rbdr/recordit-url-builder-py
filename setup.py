from setuptools import setup

setup(
        name='recordit-url-builder',
        version='1.0.0',
        author='Ben Beltran',
        author_email='ben@nsovocal.com',
        url='http://pypi.python.org/pypi/recordit-url-builder',
        license=open('LICENSE.txt').read(),
        description='Simple library to generate URLs for recordit API',
        long_description=open('README.txt').read(),
        py_modules=['recordit.url']
)
